package uk.co.bbc.numerals;

import uk.co.bbc.numerals.roman.RomanNumeralGenerator;

class InstanceConfig {

    static NumeralGenerator numeralGeneratorInstance() {
        return new RomanNumeralGenerator(new StringBuilder());
    }
}

package uk.co.bbc.numerals;

public interface NumeralGenerator {

    String generate(int number);
}

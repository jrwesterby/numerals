package uk.co.bbc.numerals;

class NumeralsApp {

    private NumeralGenerator numeralGenerator;

    NumeralsApp(NumeralGenerator numeralGenerator) {
        this.numeralGenerator = numeralGenerator;
    }

    void execute(int number) {
        try {
            System.out.println(numeralGenerator.generate(number));
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
}

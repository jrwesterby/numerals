package uk.co.bbc.numerals.roman;

import uk.co.bbc.numerals.NumeralGenerator;

public class RomanNumeralGenerator implements NumeralGenerator {

    private StringBuilder stringBuilder;

    public RomanNumeralGenerator(StringBuilder stringBuilder) {
        this.stringBuilder = stringBuilder;
    }

    public String generate(int number) {
        if (!isSupportedRange(number)) {
            throw new IllegalArgumentException("Caveat: Only support numbers between 1 and 3999");
        }
        if (number == 0) {
            String result = stringBuilder.toString();
            stringBuilder.setLength(0);
            return result;
        } else if (number >= RomanNumeral.M.getNumber()) {
            number = concatRomanNumerals(RomanNumeral.M, number);
        } else if (number >= RomanNumeral.D.getNumber()) {
            number = concatRomanNumerals(RomanNumeral.D, number);
        } else if (number >= RomanNumeral.C.getNumber()) {
            number = concatRomanNumerals(RomanNumeral.C, number);
        } else if (number >= RomanNumeral.L.getNumber()) {
            number = concatRomanNumerals(RomanNumeral.L, number);
        } else if (number >= RomanNumeral.X.getNumber()) {
            number = concatRomanNumerals(RomanNumeral.X, number);
        } else if (number >= RomanNumeral.V.getNumber()) {
            number = concatRomanNumerals(RomanNumeral.V, number);
        } else {
            number = concatRomanNumerals(RomanNumeral.I, number);
        }
        return generate(number);
    }

    private int concatRomanNumerals(RomanNumeral romanNumeral, int number) {
        if (RomanNumeralLogic.isNumeralSubtraction(number)) {
            RomanNumeral subtractedRomanNumeral = (RomanNumeralLogic
                    .isLastCaseSubtraction(number)) ? romanNumeral.getJunior() : romanNumeral;
            stringBuilder
                    .append(subtractedRomanNumeral.toString())
                    .append(romanNumeral.getSenior().toString());
            number = number - (romanNumeral.getSenior().getNumber() - subtractedRomanNumeral.getNumber());
        } else {
            stringBuilder.append(romanNumeral.toString());
            number = number - romanNumeral.getNumber();
        }
        return number;
    }

    private boolean isSupportedRange(int number) {
        return !(stringBuilder.toString().length() == 0 && (number > 3999 || number < 1));
    }
}

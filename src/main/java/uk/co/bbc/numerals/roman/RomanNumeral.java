package uk.co.bbc.numerals.roman;

import java.util.Arrays;

enum RomanNumeral {

    I(1),
    V(5),
    X(10),
    L(50),
    C(100),
    D(500),
    M(1000);

    private final int value;

    RomanNumeral(int value) {
        this.value = value;
    }

    public static RomanNumeral numeralOf(int value) {
        return Arrays.stream(values()).filter(romanNumeral -> romanNumeral.value == value).findFirst()
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format("Value '%s' does not have an explicit roman numeral", value)));
    }

    public RomanNumeral getSenior() {
        return (this.ordinal() == values().length - 1) ? values()[this.ordinal()] : values()[this.ordinal() + 1];
    }

    public RomanNumeral getJunior() {
        return (this.ordinal() == 0) ? values()[this.ordinal()] : values()[this.ordinal() - 1];
    }

    public int getNumber() {
        return value;
    }
}

package uk.co.bbc.numerals.roman;

import static java.lang.Math.log10;

class RomanNumeralLogic {

    static boolean isNumeralSubtraction(int number) {
        return (isFirstCaseSubtraction(number) || isLastCaseSubtraction(number));
    }

    static boolean isFirstCaseSubtraction(int number) {
        int firstDigit = Integer.parseInt(Integer.toString(number).substring(0, 1));
        return (log10((double) firstDigit / 4) % 1) == 0;
    }

    static boolean isLastCaseSubtraction(int number) {
        int firstDigit = Integer.parseInt(Integer.toString(number).substring(0, 1));
        return (log10((double) firstDigit / 9) % 1) == 0;
    }

}

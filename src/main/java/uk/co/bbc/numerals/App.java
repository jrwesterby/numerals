package uk.co.bbc.numerals;

import static uk.co.bbc.numerals.InstanceConfig.numeralGeneratorInstance;

public class App {

    public static void main(String[] args) {
        NumeralsApp numeralsApp = new NumeralsApp(numeralGeneratorInstance());
        numeralsApp.execute(Integer.parseInt(args[0]));
    }
}
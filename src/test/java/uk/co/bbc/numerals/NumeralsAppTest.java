package uk.co.bbc.numerals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.PrintStream;

import static org.mockito.Mockito.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class NumeralsAppTest {

    PrintStream out = mock(PrintStream.class);
    private NumeralGenerator numeralGeneratorMock = mock(NumeralGenerator.class);
    private NumeralsApp sut = new NumeralsApp(numeralGeneratorMock);

    @BeforeEach
    void setUp() {
        System.setOut(out);
    }

    @Test
    void itDelegatesToRomanNumeralGenerator() {
        sut.execute(123);
        verify(numeralGeneratorMock).generate(123);
    }

    @Test
    void itOutputsTheValueFromTheNumeralGenerator() {
        when(numeralGeneratorMock.generate(10)).thenReturn("X");
        sut.execute(10);
        verify(out).println("X");
    }

    @Test
    void whenNumeralGeneratorThrowsIllegalArgumentExceptionPrintOutMessage() {
        when(numeralGeneratorMock.generate(321)).thenThrow(new IllegalArgumentException("Error message"));
        sut.execute(321);
        verify(out).println("Error message");
    }
}
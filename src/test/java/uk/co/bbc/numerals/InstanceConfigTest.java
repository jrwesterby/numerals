package uk.co.bbc.numerals;

import org.junit.jupiter.api.Test;
import uk.co.bbc.numerals.roman.RomanNumeralGenerator;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;

class InstanceConfigTest {

    @Test
    void numeralGeneratorFactoryReturnsRomanNumeralGeneratorInstance() {
        assertThat(InstanceConfig.numeralGeneratorInstance(), instanceOf(RomanNumeralGenerator.class));
    }
}

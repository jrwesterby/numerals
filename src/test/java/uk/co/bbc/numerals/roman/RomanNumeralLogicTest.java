package uk.co.bbc.numerals.roman;


import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import uk.co.bbc.numerals.roman.RomanNumeralLogic;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RomanNumeralLogicTest {

    @Nested
    class ForNumeralsThatAreSubtractions {

        @Test
        void itReturnsTrueForOnes() {
            assertTrue(RomanNumeralLogic.isNumeralSubtraction(4));
            assertTrue(RomanNumeralLogic.isNumeralSubtraction(9));
        }
    }

    @Nested
    class ForNumeralsThatAreNotSubtractions {

        @Test
        void itReturnsFalseForOnes() {
            assertFalse(RomanNumeralLogic.isNumeralSubtraction(1));
            assertFalse(RomanNumeralLogic.isNumeralSubtraction(2));
            assertFalse(RomanNumeralLogic.isNumeralSubtraction(3));
            assertFalse(RomanNumeralLogic.isNumeralSubtraction(5));
            assertFalse(RomanNumeralLogic.isNumeralSubtraction(6));
            assertFalse(RomanNumeralLogic.isNumeralSubtraction(7));
            assertFalse(RomanNumeralLogic.isNumeralSubtraction(8));
        }
    }
}
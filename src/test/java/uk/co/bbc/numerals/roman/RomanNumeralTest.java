package uk.co.bbc.numerals.roman;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import uk.co.bbc.numerals.roman.RomanNumeral;

import static org.junit.jupiter.api.Assertions.*;

class RomanNumeralTest {

    @Nested
    class ForTranslatingANumberToANumeral {

        @Test
        void whenNumberIsOneItReturnsI() {
            assertEquals(RomanNumeral.I, RomanNumeral.numeralOf(1));
        }

        @Test
        void whenNumberIsFiveItReturnsV() {
            assertEquals(RomanNumeral.V, RomanNumeral.numeralOf(5));
        }

        @Test
        void whenNumberIsFiftyItReturnsL() {
            assertEquals(RomanNumeral.L, RomanNumeral.numeralOf(50));
        }

        @Test
        void whenNumberIsOneHundredItReturnsC() {
            assertEquals(RomanNumeral.C, RomanNumeral.numeralOf(100));
        }

        @Test
        void whenNumberIsFiveHundredItReturnsD() {
            assertEquals(RomanNumeral.D, RomanNumeral.numeralOf(500));
        }

        @Test
        void whenNumberIsOneThousandItReturnsM() {
            assertEquals(RomanNumeral.M, RomanNumeral.numeralOf(1000));
        }

        @Test
        void itThrowsAnIllegalArgumentExceptionWhenUnknownNumeral() {
            assertThrows(IllegalArgumentException.class, () -> RomanNumeral.numeralOf(123456));
        }
    }

    @Test
    void getJuniorReturnsTheNeighbourNumeralWithALowerValue() {
        assertEquals(RomanNumeral.I, RomanNumeral.I.getJunior());
        assertEquals(RomanNumeral.I, RomanNumeral.V.getJunior());
        assertEquals(RomanNumeral.V, RomanNumeral.X.getJunior());
        assertEquals(RomanNumeral.X, RomanNumeral.L.getJunior());
        assertEquals(RomanNumeral.L, RomanNumeral.C.getJunior());
        assertEquals(RomanNumeral.C, RomanNumeral.D.getJunior());
        assertEquals(RomanNumeral.D, RomanNumeral.M.getJunior());
    }

    @Test
    void getSeniorReturnsTheNeighbourNumeralWithAHigherValue() {
        assertEquals(RomanNumeral.V, RomanNumeral.I.getSenior());
        assertEquals(RomanNumeral.X, RomanNumeral.V.getSenior());
        assertEquals(RomanNumeral.L, RomanNumeral.X.getSenior());
        assertEquals(RomanNumeral.C, RomanNumeral.L.getSenior());
        assertEquals(RomanNumeral.D, RomanNumeral.C.getSenior());
        assertEquals(RomanNumeral.M, RomanNumeral.D.getSenior());
        assertEquals(RomanNumeral.M, RomanNumeral.M.getSenior());
    }

    @Test
    void toStringReturnsRomanNumeral() {
        assertEquals("X", RomanNumeral.X.toString());
    }
}
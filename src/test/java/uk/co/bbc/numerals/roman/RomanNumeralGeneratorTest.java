package uk.co.bbc.numerals.roman;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import uk.co.bbc.numerals.roman.RomanNumeralGenerator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RomanNumeralGeneratorTest {

    private RomanNumeralGenerator sut = new RomanNumeralGenerator(new StringBuilder());

    @Nested
    class ForSingleCharacters {

        @Test
        void whenNumberIsOneItReturnsI() {
            assertEquals("I", sut.generate(1));
        }

        @Test
        void whenNumberIsFiveItReturnsV() {
            assertEquals("V", sut.generate(5));
        }

        @Test
        void whenNumberIsTenItReturnsX() {
            assertEquals("X", sut.generate(10));
        }

        @Test
        void whenNumberIsFiftyItReturnsL() {
            assertEquals("L", sut.generate(50));
        }

        @Test
        void whenNumberIsOneHundredItReturnsC() {
            assertEquals("C", sut.generate(100));
        }

        @Test
        void whenNumberIsFiveHundredItReturnsD() {
            assertEquals("D", sut.generate(500));
        }

        @Test
        void whenNumberIsOneThousandItReturnsM() {
            assertEquals("M", sut.generate(1000));
        }
    }

    @Nested
    class ForCombinedCharacters {

        @Nested
        class ForOnes {
            @Test
            void whenNumberIsThreeItReturnsIII() {
                assertEquals("III", sut.generate(3));
            }

            @Test
            void whenNumberIsFourItReturnsIV() {
                assertEquals("IV", sut.generate(4));
            }

            @Test
            void whenNumberIsSixItReturnsVI() {
                assertEquals("VI", sut.generate(6));
            }

            @Test
            void whenNumberIsNineItReturnsIX() {
                assertEquals("IX", sut.generate(9));
            }
        }

        @Nested
        class ForTens {
            @Test
            void whenNumberIsThirtyItReturnsXXX() {
                assertEquals("XXX", sut.generate(30));
            }

            @Test
            void whenNumberIsFortyItReturnsXL() {
                assertEquals("XL", sut.generate(40));
            }

            @Test
            void whenNumberIsSixtyItReturnsLX() {
                assertEquals("LX", sut.generate(60));
            }

            @Test
            void whenNumberIsNinetyItReturnsXC() {
                assertEquals("XC", sut.generate(90));
            }
        }

        @Nested
        class ForHundreds {
            @Test
            void whenNumberIsThreeHundredItReturnsCCC() {
                assertEquals("CCC", sut.generate(300));
            }

            @Test
            void whenNumberIsFourHundredItReturnsCD() {
                assertEquals("CD", sut.generate(400));
            }

            @Test
            void whenNumberIsSixHundredItReturnsDC() {
                assertEquals("DC", sut.generate(600));
            }

            @Test
            void whenNumberIsNineHundredItReturnsCM() {
                assertEquals("CM", sut.generate(900));
            }
        }
    }

    @Nested
    class ForCombinations {

        @Test
        void whenOnesAreCombinedWithTens() {
            assertEquals("XCIV", sut.generate(94));
            assertEquals("LXXXIII", sut.generate(83));
        }

        @Test
        void whenOnesAreCombinedWithHundreds() {
            assertEquals("CMIV", sut.generate(904));
            assertEquals("DCCCVIII", sut.generate(808));
        }

        @Test
        void whenTensAreCombinedWithHundreds() {
            assertEquals("CMXC", sut.generate(990));
            assertEquals("DCCCX", sut.generate(810));
        }

        @Test
        void whenAllAreCombined() {
            assertEquals("MCMXCIV", sut.generate(1994));
        }
    }

    @Test
    void itThrowsAnIllegalArgumentExceptionWhenNumberIsNotBetween1and3999() {
        assertThrows(IllegalArgumentException.class, () -> sut.generate(4000));
        assertThrows(IllegalArgumentException.class, () -> sut.generate(0));
    }
}
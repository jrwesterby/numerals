### Approach
This project follows a TDD approach. I have used dependency injection which enables more efficient use a Reference Data 
Types and the ability to mock objects for test encapsulation. 

I have used a enum to define the relationship between the 
Roman numerals and decimals. This provides clarity of logic in a human readable format without documentation. These 
numerals are static and the values will rarely (never say never) change.

I was given a RomanNumeralGenerator interface to start development. I, however, change the name of this interface to 
NumeralGenerator interface to be possibly used in the future for different numerals generators. I created a factory that currently 
just spits out a RomanNumeralGenerator instance. However, this can easily be adapted to spit out different numeral generator
instances in the future (by providing lets say a flag).

I noticed a pattern for basic combinations from this site https://www.mathsisfun.com/roman-numerals.html. The pattern
repeated itself for each power of 10. This equated to a simple mathematical equation which I encapsulated in 
RomanNumeralLogic. Since the pattern was in powers of 10 I used ln (log10) in the equation. I believe that where possible
mathematical equations are the best solution since they have a constant space and time complexity O(1).

### Assumptions 
I do not tend to create abstract interfaces until I have solid proof that multiple classes conform to this standard. In
this case I made the assumption that all NumeralGenerators will generate using a decimal value. In the future this may
result in the need to use an adapter. However, the IDE does offer an ease for changing the name into something more definite 
such as DecimalToNumeralGenerator in the future. 

**(Warning - justification)** This was done because I could not see the need for the interface for the current solution and 
if I used the RomanNumeralGenerator interface I would have just ended up creating a class called RomanNumeralGeneratorImpl.
This use of Impl would state that there was a one to one relation between the interface and class (No useful polymorphism).

There was no requirement for what client was required so I went for the naive option of command line. However, I wrapped
the generator in a NumeralApp class which defines what the app does. As a result, the RomanNumeralGenerator would not need
changing. 

### Caveat
The only caveat is the one defined by the Roman Numeral logic - numbers must be between 1 and 3999.